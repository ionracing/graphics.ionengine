﻿using SDL2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Graphics.IonEngine.Events
{
    public enum KeyState
    {
        KeyUp,
        KeyDown
    }

    public class KeyEventArgs : EventArgs
    {
        public Keys KeyChar { get; set; }
        public KeyState State { get; set; }

    }
    public delegate void KeyEvent(object sender, KeyEventArgs e);
    public delegate void EventUpdater();

    public class Event
    {
        public static bool Exit { get; set; }
        public static event KeyEvent KeyDown;
        public static event KeyEvent KeyUp;
        public static event EventUpdater Updater;


        public Event()
        {
        }

        public static void DoEvents()
        {
            SDL.SDL_Event e;
            while (SDL.SDL_PollEvent(out e) != 0)
            {
                if (e.type == SDL.SDL_EventType.SDL_KEYDOWN)
                {
                    Keyboard.AddKey((Keys)e.button.button);
                    if (KeyDown != null)
                        KeyDown(null, new KeyEventArgs() { KeyChar = (Keys)e.button.button, State = KeyState.KeyDown });
                }
                else if (e.type == SDL.SDL_EventType.SDL_KEYUP)
                {
                    Keyboard.RemoveKey((Keys)e.button.button);
                    if (KeyDown != null)
                        KeyDown(null, new KeyEventArgs() { KeyChar = (Keys)e.button.button, State = KeyState.KeyUp });
                }
                else if (e.type == SDL.SDL_EventType.SDL_QUIT)
                    Exit = true;
                else if (e.type == SDL.SDL_EventType.SDL_MOUSEBUTTONDOWN)
                {
                    Mouse.AddKey((MouseButtons)e.button.button);
                    int x, y;
                    SDL.SDL_GetMouseState(out x, out y);
                    Mouse.SetMousePos(x, y);
                }
                else if (e.type == SDL.SDL_EventType.SDL_MOUSEMOTION)
                {
                    int x, y;
                    SDL.SDL_GetMouseState(out x, out y);
                    Mouse.SetMousePos(x, y);
                }
                else if (e.type == SDL.SDL_EventType.SDL_MOUSEBUTTONUP)
                {
                    Mouse.RemoveKey((MouseButtons)e.button.button);
                    int x, y;
                    SDL.SDL_GetMouseState(out x, out y);
                    Mouse.SetMousePos(x, y);
                }
            }
            if (Updater != null)
                Updater();
        }
    }
}
