﻿using Ion.Graphics.IonEngine.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Graphics.IonEngine.Events
{
    public enum MouseButtons : int
    {
        Left = 1,
        Middle = 2,
        Right = 3
    }

    public class Mouse
    {
        static int x, y;
        static List<MouseButtons> keyDown = new List<MouseButtons>();
        static MouseState? currentState = null;

        public static void SetMousePos(int x, int y)
        {
            currentState = null;
            Mouse.x = x;
            Mouse.y = y;
        }

        public static void AddKey(MouseButtons key)
        {
            currentState = null;
            if (!keyDown.Contains(key))
                keyDown.Add(key);
        }

        public static void RemoveKey(MouseButtons key)
        {
            currentState = null;
            keyDown.Remove(key);
        }

        public static MouseState GetState()
        {
            if (currentState == null)
                currentState = new MouseState(new SDLPoint(x, y), keyDown.ToArray());
            return currentState.Value;
        }
    }

    public struct MouseState
    {
        public SDLPoint Location { get; private set; }
        public MouseButtons[] Buttons { get; private set; }

        public MouseState(SDLPoint location, MouseButtons[] buttons)
        {
            this.Location = location;
            this.Buttons = buttons;
        }

        public bool IsKeyUp(MouseButtons key)
        {
            return !IsKeyDown(key);
        }

        public bool IsKeyDown(MouseButtons key)
        {
            if (Buttons == null)
                return false;
            foreach (MouseButtons keyC in Buttons)
            {
                if (keyC == key)
                    return true;
            }
            return false;
        }
    }
}
