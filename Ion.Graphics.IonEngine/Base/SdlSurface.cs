﻿using SDL2;
using System;

namespace Ion.Graphics.IonEngine
{
    public class SDLSurface : SDLBase
    {
        public SDLSurface(IntPtr basePointer)
            : base(basePointer)
        {

        }

        public static SDLSurface LoadBitmap(string path)
        {
            return new SDLSurface(SDL.SDL_LoadBMP(path));
        }

        public static SDLSurface FromPointer(IntPtr intPtr)
        {
            return new SDLSurface(intPtr);
        }

        protected override void OnDispose()
        {
            SDL.SDL_FreeSurface(BasePointer);
        }
    }
}