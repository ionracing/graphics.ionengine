﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Graphics.IonEngine
{
    public class StartableGameTime : GameTime
    {
        public void Update()
        {
            lastUpdate = thisUpdate;
            thisUpdate = TotalElapsed;
            GlobalTime = this;
        }
    }

    public class GameTime
    {
        public static GameTime GlobalTime { get; protected set; }
        protected TimeSpan lastUpdate;
        protected TimeSpan thisUpdate;

        public TimeSpan TotalElapsed
        {
            get
            {
                return TimeSpan.FromMilliseconds(SDL2.SDL.SDL_GetTicks());
            }
        }

        public TimeSpan SinceLastUpdate
        {
            get
            {
                return TotalElapsed - lastUpdate;
            }
        }

        public GameTime()
        {

        }
    }
}
