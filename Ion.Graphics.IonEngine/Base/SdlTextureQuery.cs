﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Graphics.IonEngine
{
    public struct SDLTextureQuery
    {
        public uint format;
        public int access;
        public int width;
        public int height;
    }
}
