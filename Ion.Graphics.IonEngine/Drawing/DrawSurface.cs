﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Graphics.IonEngine.Drawing
{
    public class DrawSurface : IDisposable
    {
        public byte[] pixels;
        int width, height;
        public int Width { get { return width; } }
        public int Height { get { return height; } }
        public DrawSurface(int width, int height)
        {
            this.width = width;
            this.height = height;
            pixels = new byte[width * height * 4];
        }

        ~DrawSurface()
        {
            Dispose();
        }

        public byte this[int x, int y, byte c]
        {
            get
            {
                return pixels[(x + y * width) * 4 + c];
            }
            set
            {
                if (x >= 0 && x < Width && y >= 0 && y < Height)
                {
                    pixels[(x + y * width) * 4 + c] = value;
                }
            }
        }

        public SDLColor this[int x, int y]
        {
            get
            {
                int index = (x + y * width) * 4;
                return new SDLColor(pixels[index], pixels[index + 3], pixels[index + 2], pixels[index + 1]);
            }
            set
            {
                if (x >= 0 && x < Width && y >= 0 && y < Height)
                {
                    int index = (x + y * width) * 4;
                    pixels[index] = value.A;
                    pixels[index + 3] = value.R;
                    pixels[index + 2] = value.G;
                    pixels[index + 1] = value.B;
                }
            }
        }

        public void CustomDraw(Action<int, int> action)
        {
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    action(x, y);
                }
            }
        }

        public void FillTexture(SDLTexture texture)
        {
            Marshal.Copy(pixels, 0, texture.Lock(), pixels.Length);
            texture.UnLock();
        }

        public SDLTexture MakeTexture(SDLRenderer renderer)
        {
            SDLTexture texture = SDLTexture.CreateEmpty(renderer, width, height, SDL2.SDL.SDL_PIXELFORMAT_RGBA8888);

            FillTexture(texture);
            return texture;
        }

        public bool IsDisposed { get; private set; }

        public void Dispose()
        {
            if (!IsDisposed)
            {
                pixels = null;
                IsDisposed = true;
            }
        }
    }
}
