﻿using SDL2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Graphics.IonEngine.Font
{
    public class SDLFont : SDLBase
    {
        public SDLFont(IntPtr basePointer)
            : base(basePointer)
        {

        }

        public static SDLFont LoadFont(string path, int size)
        {
            return new SDLFont(SDL_ttf.TTF_OpenFont(path, size));
        }

        protected override void OnDispose()
        {
            //TODO: find out why this is not working as it should
            //SDL_ttf.TTF_CloseFont(base.BasePointer);
        }
    }
}
