﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Graphics.IonEngine.Gui
{
    public abstract class GuiValueElement : GuiElement
    {
        protected double value;
        public double Value
        {
            get
            {
                return value;
            }
            set
            {
                if (value < 0)
                    this.value = 0;
                else if (value > MaxValue)
                    this.value = MaxValue;
                else
                    this.value = value;
            }
        }
        public double MaxValue { get; set; }

        public double Persent
        {
            get
            {
                return Value / MaxValue;
            }
        }

        public GuiValueElement()
        {
            MaxValue = 1;
        }
    }
}
