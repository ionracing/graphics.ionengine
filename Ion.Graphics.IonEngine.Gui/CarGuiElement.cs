﻿using System;
using Ion.Graphics.IonEngine.Drawing;

namespace Ion.Graphics.IonEngine.Gui
{
    public class CarGuiElement : GuiValueElement
    {
        public SDLColor FrameColor { get; set; }
        public SDLColor ValueColor { get; set; }
        public CarGuiElement()
        {
            FrameColor = SDLColor.Red;
            ValueColor = SDLColor.Red;
        }
    }
}

