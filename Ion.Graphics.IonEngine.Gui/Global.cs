﻿using System;
using Ion.Graphics.IonEngine.Font;

namespace Ion.Graphics.IonEngine.Gui
{
    /// <summary>
    /// Temporary class for not breaking everyting
    /// </summary>
    public static class Global
    {
        public static SDLFont Font 
        { 
            get
            { 
                return GuiTextElement.GlobalFont;
            }
            set
            { 
                GuiTextElement.GlobalFont = value;
            }
        }
        public static bool SideScroll { get; set; }
        public static int[,] TanPos { get; set; }
    }
}

