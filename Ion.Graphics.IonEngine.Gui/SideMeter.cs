﻿using System;
using Ion.Graphics.IonEngine.Drawing;

namespace Ion.Graphics.IonEngine.Gui
{
    public class SideMeter : CarGuiElement
    {
        SDLTexture cursor;
        SDLTexture container;

        public SideMeter()
        {
            Size = new SDLPoint(300, 40);
            Value = 0.5;
        }

        public override void Initialize(SDLRenderer renderer)
        {
            base.Initialize(renderer);
            DrawCursor(renderer);
            DrawContainer(renderer);
        }
        public void DrawCursor(SDLRenderer renderer)
        {
            DrawSurface surf = new DrawSurface(3, this.Size.Y);
            surf.CustomDraw((x, y) => surf[x, y] = ValueColor);
            cursor = surf.MakeTexture(renderer);
        }

        public void DrawContainer(SDLRenderer renderer)
        {
            DrawSurface surf = new DrawSurface(this.Size.X, this.Size.Y);
            int defaultRad = (int)Math.Sqrt((150 * 150) + (1000*1000));
            surf.CustomDraw((x, y) =>
            {
                bool lines = y == Size.Y - 1 || x == 0 || x == surf.Width - 1;
                int cirX = x - 150, cirY = y + 1000;
                int radius = (int)Math.Sqrt(cirX * cirX + cirY * cirY);
                bool radEqual = radius == defaultRad;
                bool radMore = radius >= defaultRad;
                if (lines || radEqual || (y == surf.Height - 1 && radMore))
                {
                    surf[x, y] = FrameColor;
                }
                else if (radMore)
                {
                    surf[x, y] = new SDLColor(0, 0, 0, 0);
                }
                else
                    surf[x, y] = new SDLColor(255, 0, 0, 0);
            });
            container = surf.MakeTexture(renderer);
            container.BlendMode = BlendMode.Blend;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(SDLRenderer renderer, GameTime gameTime)
        {
            renderer.DrawTexture(cursor, new SDLRectangle((int)(Persent * Size.X), 0, 3, Size.Y));
            renderer.DrawTexture(container, new SDLRectangle(0, 0, Size.X, Size.Y));
        }
    }
}

