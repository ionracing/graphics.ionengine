﻿using Ion.Graphics.IonEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Graphics.Test.EngineGround
{
    class Program
    {
        static void Main(string[] args)
        {
            MainWindow window = new MainWindow();
            window.Run();
        }
    }
}
