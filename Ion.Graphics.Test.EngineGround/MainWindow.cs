﻿using Ion.Graphics.IonEngine.Gui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ion.Graphics.IonEngine;
using System.IO;
using Ion.Graphics.IonEngine.Font;
using Ion.Graphics.IonEngine.Drawing;
using Ion.Graphics.IonEngine.Events;

namespace Ion.Graphics.Test.EngineGround
{
    public class MainWindow : GuiWindow
    {
        SDLRenderer renderer;
        SDLTexture texture;
        public SDLFont digiFont;

        public MainWindow()
        {
            renderer = SDLRenderer.Create(this);
            FileInfo fontFile = new FileInfo("Content/SFDigital.ttf");
            FileInfo font2File = new FileInfo("Content/MonospaceTypewriter.ttf");

            digiFont = SDLFont.LoadFont(fontFile.FullName, 150);
            Global.Font = SDLFont.LoadFont(font2File.FullName, 50);
            InitializeComponents(renderer);
            Global.SideScroll = true;
            ShowCursor = true;
        }
        double counter = 0;

        KeyboardState prev;
        KeyboardState cur;
        public override void Update(GameTime gameTime)
        {
            prev = cur;
            cur = Keyboard.GetState();

            if (prev.IsKeyUp(Keys.F5) && cur.IsKeyDown(Keys.F5))
                InitializeComponents(renderer);

            scroller.Update(gameTime);
            DrawSurface test = new DrawSurface(100, 100);
            int radius = test.Width / 2;
            int height = (int)(radius * Math.Cos(Math.PI / 6));
            int start = (int)(radius * Math.Sin(Math.PI / 6));
            double rel = (radius - start) / (double)height;

            //scroller.ActiveElement.Rotation = Math.PI;
            scroller.ActiveElement.Rotation += 0.0010f;
            test.CustomDraw((rX, rY) => 
            {
                if (rY > radius - height && rY < radius + height)
                {
                    double tester = Math.Abs(radius - rY); //(rY + (triHeight - radius)) / (double)triHeight;
                    if(Math.Abs(radius - rX) < (Math.Abs(height - tester) * rel + start))
                        test[rX, rY] = SDLColor.Red;
                    //if (Math.Abs(rX - radius) / triHeight > tester)

                }
            });
            /*test.CustomDraw((rX, rY) =>
            {
                int x = rX - 50;
                int y = rY - 50;
                double degree = Math.Atan2(y, -x) + Math.PI;
                double radius = Math.Sqrt(x * x + y * y);
                if (radius < 100)
                {
                    if (degree < Math.PI / 3)
                    {
                        if ((-y) + Math.Tan(Math.PI / 3) * x < 80)
                            test[rX, rY] = new SDLColor(255, 255, 255);
                    }
                    else if (degree < Math.PI / 3 * 2)
                    {
                        if (y > -40)
                            test[rX, rY] = new SDLColor(255, 0, 0);
                    }
                    else if (degree < Math.PI / 3 * 3)
                    {
                        if ((-y) + Math.Tan(Math.PI / 3 * 2) * x < 80)
                            test[rX, rY] = new SDLColor(0, 255, 0);
                    }
                    else if (degree < Math.PI / 3 * 4)
                    {
                        if ((-y) + Math.Tan(Math.PI) * x < 80)
                        {
                            test[rX, rY] = new SDLColor(0, 0, 255);
                        }
                    }
                    else if (degree < Math.PI / 3 * 5 && (-y) + Math.Tan(Math.PI + Math.PI / 6) * x < 80)
                        test[rX, rY] = new SDLColor(255, 255, 0);
                    else if (degree < Math.PI / 3 * 6 && (-y) + Math.Tan(Math.PI + Math.PI / 3) * x < 80)
                        test[rX, rY] = new SDLColor(255, 0, 255);
                }
                else
                    test[rX, rY] = new SDLColor(255, 0, 0);
            });*/
            if (texture != null)
                texture.Dispose();
            texture = test.MakeTexture(renderer);
            test.Dispose();
            //counter += 0.001;
        }

        double a = 0;
        public override void Draw(GameTime gameTime)
        {
            int raduis = (int)Math.Sqrt(50 * 50 + 50 * 50);
            a += 0.0015f;
            int x = (int)(150 - Math.Cos(a + Math.PI/4) * raduis);
            int y = (int)(150 - Math.Sin(a + Math.PI / 4) * raduis);
            renderer.Clear();
            scroller.Draw(renderer, gameTime);
            //renderer.DrawTexture(texture, new SDLRectangle(0, 0, texture.Width, texture.Height), new SDLRectangle(x,y, texture.Width, texture.Height), a, new SDLPoint(0,0), RendererFlip.None);
            renderer.Present();
        }
    }
}
